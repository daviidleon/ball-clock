# BallClock Problem

## How to use

####  npm install
  installs required dependencies
#### node {file}
  runs the selected file, available files are listed below:

## This project includes two main files:

### app.js

app.js is the calculation for the problem optimized, which includes algorithms that save computing power, using permutation vectors and only requiring the calculation of the first twelve hours of the clock, then applying the permutation vector to the ball queue until it cycles.

### clock.js

clock.js is the closest simulation for the clock problem, which solves the problem without optimization but rather following the exact algorithm a real-life clock would use.

## Description

### Clock Operation

Every minute, the least recently used ball is removed from the queue of balls at the bottom of the clock, elevated, then deposited on the minute indicator track, which is able to hold four balls. When a fifth ball rolls on to the minute indicator track, its weight causes the track to tilt. The four balls already on the track run back down to join the queue of balls waiting at the bottom in reverse order of their original addition to the minutes track. The fifth ball, which caused the tilt, rolls on down to the five-minute
indicator track. This track holds eleven balls. The twelfth ball carried over from the minutes causes the five-minute track to tilt, returning the eleven balls to the queue, again in reverse order of their addition. The twelfth ball rolls down to the hour indicator. The hour indicator also holds eleven balls, but has one extra fixed ball which is always present so that counting the balls in the hour indicator will yield an hour in the range one to twelve. The twelfth ball carried over from the five-minute indicator causes the hour indicator to tilt, returning the eleven free balls to the queue, in reverse order, before the twelfth ball itself also returns to the queue.
