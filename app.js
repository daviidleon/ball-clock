const prompt = require('prompt-sync')();

const Queue = require('./model/Queue');
const Track = require('./model/Track');

const startClock = (userInput) => {
  userInput.forEach((element) => {
    const queueSize = element;

    const ballQueue = new Queue(queueSize);
    const fiveTrack = new Track(11);
    const hourTrack = new Track(11);

    let minutes = 0;

    //Simulates 5 minutes iteration for optimization
    const tickFive = () => {
      minutes += 5;
      ballQueue.tickFive();
      if (!fiveTrack.isFull()) {
        fiveTrack.push(ballQueue.next());
      } else {
        tickHour();
      }
    };

    //Simulates the addition to the hour track, and flushes the Five Minutes Track
    const tickHour = () => {
      fiveTrack.tilt((ball) => {
        ballQueue.push(ball);
      });
      if (!hourTrack.isFull()) {
        hourTrack.push(ballQueue.next());
      } else {
        tickTwelve();
      }
    };

    //Simulates the addition back to the queue from the hour track also flushing Hour Track
    //and checks if the queue is ordered
    const tickTwelve = () => {
      hourTrack.tilt((ball) => {
        ballQueue.push(ball);
      });
      ballQueue.push(ballQueue.next());
    };

    //Generates the permutation vector analyzing the ball queue
    const generatePermutationVector = (array) => {
      const permutationVector = [];
      for (let i = 1; i <= queueSize; i++) {
        let permutatedIndex = array.findIndex((e) => e === i);
        permutationVector.push({ index: i - 1, permutatedIndex });
      }
      return permutationVector;
    };

    //Permutates the ball queue based on permutation vector
    const permutateVector = (permutationVector, array) => {
      const newArray = [...array];
      permutationVector.forEach((element) => {
        newArray[element.permutatedIndex] = array[element.index];
      });
      return newArray;
    };

    //Generates the first permutation for the ball queue (12 hours)
    for (let i = 0; i < 12 * 12; i++) {
      tickFive();
    }

    //Analyzes the ball queue and generates a permutation vector
    const permutationVector = generatePermutationVector(ballQueue.balls);

    //Permutates the ball queue until organized, adding 12 hours to the count
    while (!ballQueue.isOrdered()) {
      ballQueue.balls = permutateVector(permutationVector, ballQueue.balls);
      minutes += 720;
    }

    console.log(`${element} balls cycle after ${minutes / 1440} days`);
  });
};

// Get user input for # of balls, storing each one on array (if the number is valid)
// until eof (0)
const userInput = [];
const getInput = () => {
  let ui = parseInt(prompt('Enter the balls quantity for clock: '));
  if (ui >= 27 && ui <= 127) {
    userInput.push(ui);
    getInput();
  } else if (ui === 0) {
    startClock(userInput);
  } else {
    console.log(
      'Please enter a valid ball quantity (a number between 27 and 127)'
    );
    getInput();
  }
};

getInput();
