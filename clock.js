const prompt = require('prompt-sync')();

const Queue = require('./model/Queue');
const Track = require('./model/Track');

// Get user input for # of balls, storing each one on array (if the number is valid)
// until eof (0)

const clocksInput = [];
const getInput = () => {
  let ui = parseInt(prompt('Enter the balls quantity for clock: '));
  if (ui >= 27 && ui <= 127) {
    clocksInput.push(ui);
    getInput();
  } else if (ui === 0) {
    return;
  } else {
    console.log(
      'Please enter a valid ball quantity (a number between 27 and 127)'
    );
    getInput();
  }
};

getInput();

clocksInput.forEach((clockSize) => {
  // Initialize the Ball Queue size and Array, and the minutes
  const ballQueue = new Queue(clockSize);
  let minutes = 0;

  // Initialize class-based tracks
  const minuteTrack = new Track(4);
  const fiveTrack = new Track(11);
  const hourTrack = new Track(11);

  // Check if its ordered
  let queueOrdered = false;

  const tick = () => {
    minutes++;
    if (!minuteTrack.isFull()) {
      minuteTrack.push(ballQueue.next());
    } else {
      tickFive();
    }
  };

  const tickFive = () => {
    minuteTrack.tilt((ball) => {
      ballQueue.push(ball);
    });
    if (!fiveTrack.isFull()) {
      fiveTrack.push(ballQueue.next());
    } else {
      tickHour();
    }
  };

  const tickHour = () => {
    fiveTrack.tilt((ball) => {
      ballQueue.push(ball);
    });
    if (!hourTrack.isFull()) {
      hourTrack.push(ballQueue.next());
    } else {
      tickTwelve();
    }
  };

  const tickTwelve = () => {
    hourTrack.tilt((ball) => {
      ballQueue.push(ball);
    });
    ballQueue.push(ballQueue.next());
    queueOrdered = ballQueue.isOrdered();
  };

  do {
    tick();
  } while (!queueOrdered);

  console.log(`${clockSize} balls cycle after ${minutes / 1440} days.`);
});
