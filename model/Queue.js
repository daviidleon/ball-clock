const Track = require('./Track');

class Queue extends Track {
  constructor(size) {
    super(size);
    this.fill();
  }

  next() {
    return this.balls.shift();
  }

  fill() {
    for (let i = 1; i <= this.size; i++) {
      this.balls.push(i);
    }
  }

  isOrdered() {
    for (let i = 0; i < this.balls.length; i++) {
      if (this.balls[i] != i + 1) {
        return false;
      }
    }
    return true;
  }

  tickFive() {
    for (let i = 3; i >= 0; i--) {
      this.balls.push(this.balls[i]);
      this.balls.splice(i, 1);
    }
  }
}

module.exports = Queue;
