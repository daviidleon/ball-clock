class Track {
  constructor(size) {
    this.size = size;
    this.balls = [];
  }

  push(ball) {
    this.balls.push(ball);
  }

  isFull() {
    return this.size == this.balls.length;
  }

  tilt(cb) {
    let length = this.balls.length;
    for (let i = 0; i < length; i++) {
      cb(this.balls.pop());
    }
  }

  log() {
    console.log(this.balls);
  }
}

module.exports = Track;
